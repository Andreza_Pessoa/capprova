
//variavel global para guardar os dados
var iRPF = []
//criando botao
//capturando informações da tela

var btnCalcular = document.getElementById("btnCalcular")
btnCalcular.addEventListener('click', function (event) {

    var dadosIRPF = []

    var codigo = document.getElementById("idCodigo").value;
    dadosIRPF.push(codigo)

    var funcionario = document.getElementById("idFuncionario").value;
    dadosIRPF.push(funcionario)

    var cargo = document.getElementById("idCargo").value;
    dadosIRPF.push(cargo)

    var salarioBruto =Number(document.getElementById("idSalarioBruto").value);
    dadosIRPF.push(salarioBruto)

    var salarioImposto = calcularImposto(salarioBruto)
    dadosIRPF.push(salarioImposto)

    var salarioLiq = calcularSalarioLiquido(salarioImposto, salarioBruto)
    dadosIRPF.push(salarioLiq)

    iRPF.push(dadosIRPF)
    console.log(iRPF);

})
//criando o calculo para o irpf

function calcularImposto(salarioBruto) {

    let salarioImposto
    var msn = 0

    if (salarioBruto < 1903.98) {
        salarioImposto = msn
    } else if (salarioBruto >= 1903.99 && salarioBruto <= 2826.65) {
        salarioImposto = ((salarioBruto * 7.5) / 100)
    } else if (salarioBruto >= 2826.66 && salarioBruto <= 375105) {
        salarioImposto = ((salarioBruto * 15) / 100)
    } else if (salarioBruto >= 3751.06 && salarioBruto <= 4664.68) {
        salarioImposto = ((salarioBruto * 22.5) / 100)
    } else {
        salarioImposto = ((salarioBruto * 27.5) / 100)
    }
    return salarioImposto
    // console.log(salarioImposto);

}

//criar calculo para o salario liquido

function calcularSalarioLiquido(salarioImposto, salarioBruto) {


    var salarioLiq
   

    if (salarioBruto < 1903.98) {
        salarioLiq  = salarioBruto
    } else if (salarioBruto >= 1903.99 && salarioBruto <= 2826.65) {
      salarioLiq=  salarioBruto - salarioImposto
    } else if (salarioBruto >= 2826.66 && salarioBruto <= 375105) {
       salarioLiq = salarioBruto - salarioImposto
    } else if (salarioBruto >= 3751.06 && salarioBruto <= 4664.68) {
        salarioLiq =  salarioBruto - salarioImposto
    } else {
        salarioLiq =  salarioBruto - salarioImposto
    }
    return  salarioLiq
   

}


//enviar para a tela


function exibirRelatorio() {

    console.log("oiii");
    iRPF.sort(function (a, b) {
        if (a[4] > b[4]) {
            return 1
        }

        if (b[4] > a[4]) {
            return -1
        }

        return 0
    })

    var tbodyRel = document.getElementById("tboIdLinha")
    for (let i = 0; i < iRPF.length; i++) {
        var linha = montaTr(iRPF[i])
        tbodyRel.appendChild(linha)
    }
}





//Cria UMA linha (um tr)
function montaTr(dadosIRPF) {

    let impostoTr = document.createElement("tr")
    impostoTr.classList.add("trClUsuarioData")

    impostoTr.appendChild(montaTd(dadosIRPF[0], "tdUserCod"))
    impostoTr.appendChild(montaTd(dadosIRPF[1], "tdUserFuncionario"))
    impostoTr.appendChild(montaTd(dadosIRPF[2], "tdUserCargo"))
    impostoTr.appendChild(montaTd(dadosIRPF[3], "tdUserSalarioB"))
    impostoTr.appendChild(montaTd(dadosIRPF[4].toFixed(2), "tdUserIRPF"))
    impostoTr.appendChild(montaTd(dadosIRPF[5].toFixed(2), "tdUserSalarioL"))

    return impostoTr
}


//Cria UMA coluna (um td)
function montaTd(dado, classe) {
    let impostoTd = document.createElement("td")
    impostoTd.classList.add(classe)

    impostoTd.textContent = dado

    return impostoTd
}